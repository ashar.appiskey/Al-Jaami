import 'package:flutter/foundation.dart';

class APIBase {
  static String get baseURL {
    if (kReleaseMode) {
      return "https://vipankumar.com/SmartHealth/api/";
    } else {
      return "https://vipankumar.com/SmartHealth/api/";
    }
  }

  static String get clientSecret {
    if (kReleaseMode) {
      return "Production Client Secret";
    } else {
      return "Dev Client Secret";
    }
  }

  static int get clientId {
    return 2;
  }
}
