import '../enums/index.dart';

class APIPathHelper {
  static String getValue(APIPath path) {
    switch (path) {
      case APIPath.countries:
        return "getCountries";
      default:
        return "";
    }
  }
}
