import 'package:aljaami/model/countries_model.dart';
import 'package:aljaami/sources/helper_methods.dart';
import 'package:aljaami/widgets/text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomListTile extends StatelessWidget {
  const CustomListTile({
    Key? key,
    required this.list,
    required this.index,
    this.verticalPadding = 1,
    this.horizontalPadding = 2,
    this.containerRadius = 1,
    required this.onPressed,
    this.radius = 40,
    this.buttonWidth = 6.5,
    this.buttonHeight = 6.5,
    this.counterWidth = 1.3,
    this.counterHeight = 0.3,
  }) : super(key: key);

  final List<Country> list;
  final int index;
  final double verticalPadding;
  final double horizontalPadding;
  final double containerRadius;
  final Function onPressed;
  final double radius;
  final double buttonWidth;
  final double buttonHeight;
  final double counterWidth;
  final double counterHeight;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: Helper.dynamicHeight(context, verticalPadding),
        horizontal: Helper.dynamicWidth(context, horizontalPadding),
      ),
      child: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              spreadRadius: 1,
              color: Colors.black87.withOpacity(0.1),
              blurRadius: 10,
              offset: const Offset(2, 2),
            )
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(
            Helper.dynamicFont(context, containerRadius),
          ),
          child: Material(
            child: InkWell(
              onTap: () => onPressed(),
              child: Container(
                padding: EdgeInsets.symmetric(
                  vertical: Helper.dynamicHeight(context, 2),
                  horizontal: Helper.dynamicWidth(context, 4),
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextWidget(
                            text: list[index].countryName,
                            fontSize: 1.25,
                            maxLines: 1,
                            overFlow: TextOverflow.ellipsis,
                            textAlign: TextAlign.start,
                            color:
                                Theme.of(context).textTheme.subtitle2!.color!,
                            fontWeight: FontWeight.w600,
                          ),
                          SizedBox(
                            height: Helper.dynamicHeight(context, .5),
                          ),
                          TextWidget(
                            text: list[index].countryCode,
                            fontSize: 1,
                            color: const Color.fromRGBO(101, 101, 101, 1),
                            maxLines: 2,
                            overFlow: TextOverflow.ellipsis,
                            textAlign: TextAlign.start,
                          ),
                        ],
                      ),
                    ),
                    TextWidget(
                      text: list[index].phoneCode,
                      fontSize: 1,
                      maxLines: 1,
                      overFlow: TextOverflow.ellipsis,
                      textAlign: TextAlign.start,
                      color: Theme.of(context).textTheme.subtitle2!.color!,
                      fontWeight: FontWeight.w600,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
