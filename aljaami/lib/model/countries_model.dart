// To parse this JSON data, do
//
//     final countriesApi = countriesApiFromJson(jsonString);

import 'dart:convert';

CountriesApi countriesApiFromJson(String str) => CountriesApi.fromJson(json.decode(str));

String countriesApiToJson(CountriesApi data) => json.encode(data.toJson());

class CountriesApi {
    CountriesApi({
       required this.status,
       required this.message,
       required this.data,
    });

    bool status;
    String message;
    Data data;

    factory CountriesApi.fromJson(Map<String, dynamic> json) => CountriesApi(
        status: json["status"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data.toJson(),
    };
}

class Data {
    Data({
       required this.countries,
    });

    List<Country> countries;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        countries: List<Country>.from(json["countries"].map((x) => Country.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "countries": List<dynamic>.from(countries.map((x) => x.toJson())),
    };
}

class Country {
    Country({
       required this.countryId,
       required this.countryName,
       required this.countryCode,
       required this.phoneCode,
       required this.isoCode3,
       required this.status,
       required this.image,
    });

    String countryId;
    String countryName;
    String countryCode;
    String phoneCode;
    String isoCode3;
    String status;
    String image;

    factory Country.fromJson(Map<String, dynamic> json) => Country(
        countryId: json["country_id"],
        countryName: json["country_name"],
        countryCode: json["country_code"],
        phoneCode: json["phone_code"],
        isoCode3: json["iso_code_3"],
        status: json["status"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "country_id": countryId,
        "country_name": countryName,
        "country_code": countryCode,
        "phone_code": phoneCode,
        "iso_code_3": isoCode3,
        "status": status,
        "image": image,
    };
}
