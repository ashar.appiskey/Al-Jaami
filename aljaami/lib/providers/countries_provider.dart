import 'dart:async';

import 'package:aljaami/enums/index.dart';
import 'package:aljaami/model/countries_model.dart';
import 'package:aljaami/network_module/api_response.dart';
import 'package:aljaami/repo/countries_repo.dart';
import 'package:flutter/material.dart';

class CountriesProvider with ChangeNotifier {
  late Timer _timer;

  List<Country> countriesList = [];

  late CountriesRepository _countriesRepository;

  late ApiResponse<CountriesApi> _countries;

  ApiResponse<CountriesApi> get countries => _countries;

  CountriesProvider() {
    _countriesRepository = CountriesRepository();
    _countries = ApiResponse<CountriesApi>();
  }

  startTimer() {
    _timer = Timer.periodic(const Duration(seconds: 10), (_) {
      if (countriesList.isNotEmpty ) {
        print("items removed");
        // ignore: list_remove_unrelated_type
        countriesList.clear();
        notifyListeners();
      }
    });
  }

  resetTimer() {
    if (_timer.isActive) {
      _timer.cancel();
      notifyListeners();
    }
  }

  fetchCountries() async {
    _countries = ApiResponse.loading('loading... ');
    countriesList.clear();
    notifyListeners();
    try {
      CountriesApi countries = await _countriesRepository.fetchCountriesList();
      countriesList = countries.data.countries;
      _countries = ApiResponse.completed(countries);
      notifyListeners();
    } catch (e) {
      print(e.toString());
      _countries = ApiResponse.error(e.toString());
      notifyListeners();
    }
  }
}
