import 'package:aljaami/providers/countries_provider.dart';
import 'package:aljaami/sources/shared_preferences.dart';
import 'package:aljaami/views/home_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main()async {
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPreferenceManager.init();
  runApp(
    MultiProvider(providers: [
      ChangeNotifierProvider.value(value: CountriesProvider()),
    ], child: const MyApp()),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Al-Jaami',
      home: HomePage(),
    );
  }
}
