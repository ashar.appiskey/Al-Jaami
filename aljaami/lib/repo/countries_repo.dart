import 'package:aljaami/enums/index.dart';
import 'package:aljaami/model/countries_model.dart';
import 'package:aljaami/network_module/api_path.dart';
import 'package:aljaami/network_module/http_client.dart';

class CountriesRepository {
  Future<CountriesApi> fetchCountriesList() async {
    final response = await HttpClient.instance
        .fetchData(APIPathHelper.getValue(APIPath.countries));
    return CountriesApi.fromJson(response);
  }
}
