import 'package:aljaami/enums/index.dart';
import 'package:aljaami/providers/countries_provider.dart';
import 'package:aljaami/sources/helper_methods.dart';
import 'package:aljaami/widgets/custom_list_tile.dart';
import 'package:aljaami/widgets/text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final ScrollController _conversationsListController;
  @override
  void initState() {
    _conversationsListController = ScrollController();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      getCountries();
    });
    super.initState();
  }

  Future<void> getCountries() async {
    await Provider.of<CountriesProvider>(context, listen: false)
        .fetchCountries();

    if (Provider.of<CountriesProvider>(context, listen: false)
            .countries
            .status ==
        Status.LOADING) {
      // reset the timer
      Provider.of<CountriesProvider>(context, listen: false).resetTimer();
    }

    if (Provider.of<CountriesProvider>(context, listen: false)
            .countries
            .status ==
        Status.COMPLETED) {
      //start the timer to remove items after every 10 seconds
      Provider.of<CountriesProvider>(context, listen: false).startTimer();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: EdgeInsets.all(Helper.dynamicFont(context, 1)),
            child: TextWidget(
              text: "Countries List",
              fontSize: 2,
              color: Theme.of(context).textTheme.subtitle2!.color!,
              fontWeight: FontWeight.w600,
            ),
          ),
          Expanded(
            child: RefreshIndicator(
              child: _countriesListContainer(context),
              triggerMode: RefreshIndicatorTriggerMode.anywhere,
              onRefresh: () => getCountries(),
            ),
          )
        ]),
      ),
    );
  }

  _countriesListContainer(BuildContext context) {
    return Consumer<CountriesProvider>(
      builder: (context, countrylist, child) {
        if (countrylist.countries.status == Status.COMPLETED ||
            countrylist.countries.status == Status.LOADING_MORE) {
          if (countrylist.countriesList.isNotEmpty) {
            return ListView.builder(
              physics: const BouncingScrollPhysics(),
              controller: _conversationsListController,
              padding: EdgeInsets.only(
                top: 0,
                bottom: Helper.dynamicHeight(context, 10),
              ),
              itemCount: countrylist.countriesList.length,
              itemBuilder: (context, index) {
                return CustomListTile(
                  list: countrylist.countriesList,
                  index: index,
                  onPressed: () {},
                  horizontalPadding: 3.5,
                );
              },
            );
          } else {
            return ListView(
              children: [
                TextWidget(
                  text: 'No Countries pull to refresh',
                  color: Theme.of(context).textTheme.subtitle2!.color,
                ),
              ],
            );
          }
        } else if (countrylist.countries.status == Status.LOADING) {
          return const Center(
            child: CircularProgressIndicator(
              value: 0.8,
              valueColor: AlwaysStoppedAnimation<Color>(Colors.purple),
            ),
          );
        } else if (countrylist.countries.status == Status.ERROR) {
          return Center(
              child: TextWidget(
            text: 'Error Fetching API Data',
            color: Theme.of(context).textTheme.subtitle2!.color,
          ));
        } else {
          return Center(
              child: TextWidget(
            text: 'Something went wrong',
            color: Theme.of(context).textTheme.subtitle2!.color,
          ));
        }
      },
    );
  }
}
